Func<IObservable<BigInteger>, Func<IObservable<BigInteger>, IObservable<BigInteger>>, IObservable<BigInteger>> consMap =
  (startWith, function) => Observable.Create<BigInteger>(observer => 
	{	
		var subject = new Subject<BigInteger>();			
		var loopFunc = function(subject.ObserveOn(Scheduler.NewThread));
		var loopSub  = loopFunc.Subscribe(i => subject.OnNext(i));			
		var outerSub = subject.Subscribe(i => observer.OnNext(i));			
		startWith.Subscribe(subject.OnNext);				
		return new CompositeDisposable(2) { loopSub, outerSub };
	});	

var fibs = consMap(new BigInteger[] { 0, 1 }.ToObservable(), xs => xs.Zip(xs.Skip(1), (x, y) => x + y)); 

fibs.Take(5000).Dump();
