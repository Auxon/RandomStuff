<Query Kind="Statements">
  <NuGetReference>Rx-Main</NuGetReference>
  <Namespace>System</Namespace>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Concurrency</Namespace>
  <Namespace>System.Reactive.Disposables</Namespace>
  <Namespace>System.Reactive.Joins</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Reactive.PlatformServices</Namespace>
  <Namespace>System.Reactive.Subjects</Namespace>
  <Namespace>System.Reactive.Threading.Tasks</Namespace>
</Query>

var xs = new []{ -3, 0, 2, 4, -2 }.OrderBy (x => x);
var zs = 
xs.Zip(xs.Skip(1)
	.Zip(xs.Skip(2), 
	(y, z) => new { y, z }),
	(x, s) => new { x, s.y, s.z })
	.Where (t => t.x + t.y + t.z == 0)
	.Select (t => t);
zs.Dump();
