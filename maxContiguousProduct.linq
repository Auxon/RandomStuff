<Query Kind="Statements">
  <NuGetReference>Rx-Main</NuGetReference>
  <Namespace>System</Namespace>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Concurrency</Namespace>
  <Namespace>System.Reactive.Disposables</Namespace>
  <Namespace>System.Reactive.Joins</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Reactive.PlatformServices</Namespace>
  <Namespace>System.Reactive.Subjects</Namespace>
  <Namespace>System.Reactive.Threading.Tasks</Namespace>
</Query>

int[] arr = {6,-7,8,-9,8,-7,6};

Func<int[], int?> maxContiguous = (xs) => {
	if(xs == null || xs.Length == 0) return null;
	if(xs.Length == 1) return xs[0];
	
	int max = 0;
	int n = 0;
	for(var i=1; i <= arr.Length; i++) {
		var p = xs.Skip(n).Take(i).Aggregate ((x, y) => x * y);
		if(p >= max) {
			max = p;	
		} else {
			n++;i--;
		}
	}	
	return max;
};

maxContiguous(arr).Dump();

maxContiguous(new [] { 1, 2, -3, 3, 1, 4}).Dump();

Func<float[], float> maxOf = (xs) => {
	float maxS = xs[0];
	float minS = xs[0];
	float maxVal = maxS;
	for (int i = 0; i < xs.Length; i++)
	{
		float prevMax = maxS;
		float prevMin = minS;
		maxS = new [] { xs[i], xs[i]*prevMin, xs[i]*prevMax }.Max();
		minS = new [] { xs[i], xs[i]*prevMin, xs[i]*prevMax }.Min();
		maxVal = Math.Max(maxS, minS);
	}
	return maxVal;
};


maxOf(new [] { 1f, 2f, -3f, 3f, 1f, 4f}).Dump();