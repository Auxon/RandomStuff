<Query Kind="Program">
  <NuGetReference>Rx-Main</NuGetReference>
  <Namespace>System</Namespace>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Concurrency</Namespace>
  <Namespace>System.Reactive.Disposables</Namespace>
  <Namespace>System.Reactive.Joins</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Reactive.PlatformServices</Namespace>
  <Namespace>System.Reactive.Subjects</Namespace>
  <Namespace>System.Reactive.Threading.Tasks</Namespace>
</Query>

void Main()
{
	Tree<int> root = new Tree<int> { Value = 234 };
	root.Right(5).Parent.Left(2).Left(6);
	Debug.Assert(root.Right.Value == 5);
	Debug.Assert(root.Left.Value == 2);
	Debug.Assert(root.Left.Left.Value == 6);
	
	root.PreorderTraversal();
	
	var t = new Tree<int>();
	t.Left(5).Left(7);
	t.Left.Left.Value.Dump();
	t.Right(124);
	
	t.Left(7).Value.Dump();
	
	t.Left.Value.Dump();
	
	t.PreorderTraversal();
}

// Define other methods and classes here
class Tree<T> {
	public Tree<T> Right { get; set; }	
	public Tree<T> Left { get; set; }	
	public Tree<T> Parent { get; set; }
	public T Value { get ; set; }
}

static class TreeExt {
	public static Tree<T> Right<T>(this Tree<T> t, T value) {
		if(t.Right == null) {
			t.Right = new Tree<T> { Parent = t, Value = value };
		} 
		else {
			t.Right.Parent = t;
			t.Right.Value = value;
		}
		return t.Right;
	}
	public static Tree<T> Left<T>(this Tree<T> t, T value) {
		if(t.Left == null) {
			t.Left = new Tree<T> { Parent = t, Value = value };
		}
		else {
			t.Left.Parent = t;
			t.Left.Value = value;
		}
		return t.Left;
	}
	public static void PreorderTraversal<T>(this Tree<T> t) {
		if(t == null) throw new ArgumentNullException("t");
		t.Value.Dump();
		if(t.Left != null) PreorderTraversal(t.Left);
		if(t.Right != null) PreorderTraversal(t.Right);
		
	}
}