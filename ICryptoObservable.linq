<Query Kind="Program">
  <Output>DataGrids</Output>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.Formatters.Soap.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.Json.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.Primitives.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.Xml.dll</Reference>
  <NuGetReference>System.Reactive.Core</NuGetReference>
  <NuGetReference>System.Reactive.Interfaces</NuGetReference>
  <NuGetReference>System.Reactive.Linq</NuGetReference>
  <NuGetReference>System.Reactive.Observable.Aliases</NuGetReference>
  <NuGetReference>System.Reactive.Providers</NuGetReference>
  <Namespace>System</Namespace>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Concurrency</Namespace>
  <Namespace>System.Reactive.Disposables</Namespace>
  <Namespace>System.Reactive.Joins</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Reactive.PlatformServices</Namespace>
  <Namespace>System.Reactive.Subjects</Namespace>
  <Namespace>System.Reactive.Threading.Tasks</Namespace>
  <Namespace>System.Runtime.Serialization</Namespace>
  <Namespace>System.Runtime.Serialization.Json</Namespace>
  <Namespace>System.Security.Cryptography</Namespace>
  <Namespace>System.Xml</Namespace>
  <Namespace>System.Runtime.Serialization.Formatters.Binary</Namespace>
</Query>

void Main()
{
	// Proof of Concept.  Jan., 01, 2015.  rah
	
	// CryptoLambdaIO is a stream of lambda expressions that may come from anywhere, and forms the base of what
	// is a type of distributed dispatch system for constructing secure and trusted applications that have a built
	// in payment, versioning, redundancy and copyright protection system.
	
	// Code will execute on the Bitcoin protocol, using coins to prevent a whole range of 
	// attack vectors and create a new kind of distributed programming environment for developers where code is shared
	// and any unique contribution of code provided by any user anywhere will yield automatic royalties to the original developers
	// and residuals to forkers or maintainers.
	
	// In a distributed environment, such as the "Internet of Things" - IoT,  these instructions are completely
	// untrustable without something like the Bitcoin protocol.  We also don't want any middle man attacks or other
	// vectors to somehow interfere with the execution of a distributed program.
	
	// Limitations of the C# 5 compiler don't allow lambda expressions assigned to implicitly typed locals or anonymous type
	// properties.  
	
	var instructions = new Dictionary<int, LambdaExpression>();	
	Expression<Func<int, int, int>> add = (x, y) => x + y;
	Expression<Func<int, int, int>> multiply = (x, y) => x * y;
	instructions.Add(add.GetHashCode(), (LambdaExpression)add);
	instructions.Add(multiply.GetHashCode(), (LambdaExpression)multiply);
	instructions.Dump("The instructions:");
	
	var ico = Observable.Create<LambdaExpression>(o => {		
		o.OnNext(add);
		o.OnNext(multiply);
		o.OnCompleted();		
		return Disposable.Empty; 
	});
	
	using(var aes = Aes.Create()) {
		var encryptedIO = ico.Encrypt(aes.Key, aes.IV);
		
		// Note:  The signature of the function must be known as well or it will be the wrong type.
		var plaintextIO = encryptedIO.Decrypt<Func<int, int, int>>(aes.Key, aes.IV);
		
		// attempt to use a bad key
		using(var badAes = Aes.Create()) {
			plaintextIO = encryptedIO.Decrypt<Func<int, int, int>>(badAes.Key, badAes.IV);
			plaintextIO.Subscribe(f => f(6, 7).Dump());
		}
		
		plaintextIO.Subscribe(f => f(6, 7));		
		
		Console.ReadLine();
	}
}

public static class EncryptionExtensions {
	public static IObservable<byte[]> Encrypt<T>(this IObservable<T> source, byte[] key, byte[] IV) where T:LambdaExpression {		
		return Observable.Create<byte[]>(o =>
			source.Subscribe(t => {
				using(var aes = Aes.Create()) {
					var encryptor = aes.CreateEncryptor(key, IV);					
					using (var ms = new MemoryStream()) {
						using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write)) {
							//TODO:  Serialization issues with ExpressionTrees.  I just need LambdaExpressions
							// to be serializable, but apparently this isn't as simple as it sounds.
							// https://social.msdn.microsoft.com/Forums/en-US/cf4eab1b-98d7-4049-9bee-3b43b1cf7608/why-expression-trees-are-not-serializable?forum=linqprojectgeneral
							var formatter = new System.Runtime.Serialization.Json.DataContractJsonSerializer(t.GetType(), "root");
							
							formatter.WriteObject(cs, t);					
						}
						var encrypted = ms.ToArray();
						o.OnNext(encrypted);
					}
      			}
			})
		);
	}
	
	public static IObservable<T> Decrypt<T>(this IObservable<byte[]> source, byte[] key, byte[] IV) {		
		return Observable.Create<T>(o =>
			source.Subscribe(t => {
				using(var aes = Aes.Create()) {
					var decryptor = aes.CreateDecryptor(key, IV);												
					using (var ms = new MemoryStream(t)) {
						using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read)) {
							var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
							T decrypted = (T)formatter.Deserialize(cs);
							try {
								o.OnNext(decrypted);
							} catch(CryptographicException cex) {
								o.OnError(cex);
							}
						}						
					}
      			}
			})
		);
	}
}