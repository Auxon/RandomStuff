<Query Kind="Program">
  <NuGetReference>Rx-Main</NuGetReference>
  <NuGetReference>Rx-Testing</NuGetReference>
  <Namespace>Microsoft.Reactive.Testing</Namespace>
  <Namespace>System</Namespace>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Concurrency</Namespace>
  <Namespace>System.Reactive.Disposables</Namespace>
  <Namespace>System.Reactive.Joins</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Reactive.PlatformServices</Namespace>
  <Namespace>System.Reactive.Subjects</Namespace>
  <Namespace>System.Reactive.Threading.Tasks</Namespace>
</Query>

void Main()
{
	var s = new TestScheduler();
	s.Schedule(1, (_, state) => {
		state.Dump();
		return Disposable.Empty;
	});
	s.Schedule(2, TimeSpan.FromTicks(100), (_, state) => {
		state.Dump();
		return Disposable.Empty;
	});
	//s.AdvanceBy(1);
	//s.AdvanceBy(1);
	//s.AdvanceBy(97);
	//s.AdvanceBy(1);
	var obs = s.Start(() => Observable.Return(24), 1, 2, 3);
	obs.Messages.Dump();
	
}

// Define other methods and classes here
